#!/bin/bash

TAG_SUFFIX="$(git branch --show-current)-WIP"

docker stack rm rllib
sleep 10

echo "Building Head:"
docker build --build-arg TAG_SUFFIX="$TAG_SUFFIX" --tag "multiplatformautonomy/vscrimmage-rllib:head-$TAG_SUFFIX" rllib_head
docker push "multiplatformautonomy/vscrimmage-rllib:head-$TAG_SUFFIX"

echo -e "\nBuilding Worker:"
docker build --build-arg TAG_SUFFIX="$TAG_SUFFIX" --tag "multiplatformautonomy/vscrimmage-rllib:worker-$TAG_SUFFIX" rllib_worker
docker push "multiplatformautonomy/vscrimmage-rllib:worker-$TAG_SUFFIX"

docker stack deploy -c docker-compose.yml rllib
sleep 2
for task in rllib_head rllib_worker; do docker service logs $task -t; done
