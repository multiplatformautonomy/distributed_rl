#!/bin/bash

docker stack rm rllib
sudo rm -rf logs
mkdir -p logs/head logs/worker
# shellcheck disable=SC2046
docker rmi $(docker images | grep "^multiplatformautonomy" | awk "{print $3}")
