#!/bin/bash

echo "Waiting for $HEAD_ADDRESS:$REDIS_PORT to be up."

# Wait for head to be awake or crash after 90 seconds.
wait-for-it.sh "$HEAD_ADDRESS:$REDIS_PORT" -t 90 || exit

MY_ADDRESS="$(hostname -i)"

if [ -z "$VERBOSE" ]; then
    echo -e "Staring worker...";
else
    echo "Starting worker..."
    echo "Head address:   $HEAD_ADDRESS"
    echo "Redis port:     $REDIS_PORT"
    echo "Redis password: $REDIS_PASSWORD"
fi

ray start --address="$HEAD_ADDRESS:$REDIS_PORT" --redis-password="$REDIS_PASSWORD" --node-ip-address="$MY_ADDRESS"
tail -f /dev/null
