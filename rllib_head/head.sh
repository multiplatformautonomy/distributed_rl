#!/bin/bash

export PATH="$PATH:~/.local/bin"
MY_ADDRESS="$(hostname -i)"

if [-z "$VERBOSE" ]; then
    echo -e "Performing setup..."
else
    echo -e "Performing setup..."
    echo -e "Disabling IPv6 Hostnames to avoid 'ray start' bugs..."
fi
mv /etc/hosts /etc/hosts.bup
sed -e 's/^\(.*::.*\)$/# \1/g' /etc/hosts.bup | tee /etc/hosts

if [ ! -z "$VERBOSE" ]; then
    echo -e "Setting Jupyter Lab password..."
fi
echo -e "$JUPYTER_PASSWORD\n$JUPYTER_PASSWORD\n" | jupyter notebook password

if [ -z "$VERBOSE" ]; then
    echo -e "Staring head node...";
else
    echo "Starting head node..."
    echo "Redis password: $REDIS_PASSWORD"
    echo "Jupyter Lab Password: $JUPYTER_PASSWORD"
    echo "Docker Swarm IP: $MY_ADDRESS"
fi

ulimit -c unlimited
ray start --head --redis-password="$REDIS_PASSWORD" --node-ip-address "$MY_ADDRESS"
jupyter lab --ip 0.0.0.0 --port 3888
