# Distributed RL with SCRIMMAGE

## Usage

Type into your terminal:
```
git clone https://gitlab.com/multiplatformautonomy/distributed_rl.git
docker stack deploy -c docker-compose.yml
```

Type into your browser:
```
http://localhost:3888
```
You should see a notebook environment for your distributed RL development.